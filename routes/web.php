<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::delete('comanda/{id}', 'ComandaController@destroy')->name('comanda.destroy');
// Route::post('comanda/{id}', 'ComandaController@update')->name('comanda.update');
// Route::get('comanda/{id}/edit', 'ComandaController@edit')->name('comanda.edit');
// Route::get('comanda/create', 'ComandaController@create')->name('comanda.create');
// Route::get('comanda/{id}', 'ComandaController@show')->name('comanda.show');
// Route::get('comanda', 'ComandaController@index')->name('comanda.index');
// Route::post('comanda', 'ComandaController@store')->name('comanda.store');
Route::resource('comanda', 'ComandaController');